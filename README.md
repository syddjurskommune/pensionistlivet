Pensionistlivet was developed by Syddjurs Kommune (http://www.syddjurs.dk).

Author: Kristian Hansen (kbha@syddjurs.dk)

License and Copyright

Copyright (c) 2019, Syddjurs Kommune.

All source code in this and the underlying directories is subject to the terms of the Mozilla Public License, v. 2.0. See the LICENSE file for details. If a copy of the MPL was not distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.