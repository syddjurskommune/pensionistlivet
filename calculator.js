﻿$(function () {
    $('[data-toggle="tooltip"]').tooltip();

    $('input[name="HasHealthAllowance"]').change(function () {
        healthAllowanceToggleVisibility();
    });

    $("#AllowancePercentage, #Fortune").keyup(function (e) {
        healthAllowanceToggleVisibility();
    });
});

function healthAllowanceToggleVisibility() {
    var hasHealthAllowance = $('input[name="HasHealthAllowance"]:checked').val() === "True";
    var allowancePercentage = parseInt($("#AllowancePercentage").val());
    var fortune = parseInt($("#Fortune").val());

    if (hasHealthAllowance === false && allowancePercentage > 0 && fortune < 86000) {
        $("#health-allowance-breaker").removeClass("d-none");
    } else {
        $("#health-allowance-breaker").addClass("d-none");
    }
}