﻿$(function () {
    $("#accordion").on("shown.bs.collapse", function (e) {
        var lastCollapsibleNavigationItem = $(".collapsible-navigation-item-link:last").attr("data-target").replace("#", "");
        var id = $(e.target).attr("id");

        if (id === lastCollapsibleNavigationItem) {
            window.scrollTo(0, document.body.scrollHeight);
        }
    });
});